module.exports = {
    publicClasses: function(context, options) {
        'use strict';
        var ret = "";

        for(var i=0; i < context.length; i++) {
            if(!context[i].itemtype && context[i].access === 'public') {
                ret = ret + options.fn(context[i]);
            } else if (context[i].itemtype) {
                ret = ret + options.fn(context[i]);
            }
        }

        return ret;
    },
    /**
     * Hack for:
     * https://github.com/yui/yuidoc/issues/198
     *
     * Usage:
     *   {{#crossLinkWrapper type}}{{#crossLink type}}{{/crossLink}}{{/crossLinkWrapper}}
     */
    crossLinkWrapper: function(context, options) {
        if (!context) return '';
        var linkedType, url;
        var types_raw = context.split('|');
        var types_linked = options.fn(this).split('|');

        return types_raw.map(function(type, i) {
            if (/\[\]/.test(type)) {
                if (/<\/a>/.test(types_linked[i])) {
                    return convertATagtoMarkupLink(types_linked[i].replace('</a>', '[]</a>'));
                } else {
                    return convertATagtoMarkupLink(types_linked[i].trim() + '[]');
                }
            } else {
                linkedType = types_linked[i].match(/>(.*?)</);
                return convertATagtoMarkupLink(types_linked[i]);
            }
        }).join(' | ');
    },
    notEmpty: function(context, options) {
        'use strict';
        if (!context || !String(context).replace(/\s/g, '')) {
            return options.inverse(this);
        }
        return options.fn(this);
    },
    hasStaticMethods: function(context, options) {
        'use strict';
        var hasStatic = false;
        if (!context) return '';
        for(var i=0; i < context.length; i++) {
            if (context[i]['static']) {
                hasStatic = true;
                break;
            }
        }
        if (hasStatic) {
            return options.fn(this);
        }
        return '';
    },
    hasInstanceMethods: function(context, options) {
        'use strict';
        var hasInstance = false;
        if (!context) return '';
        for(var i=0; i < context.length; i++) {
            if (!context[i]['static']) {
                hasInstance = true;
                break;
            }
        }
        if (hasInstance) {
            return options.fn(this);
        }
        return '';
    },
    search: function(classes, modules) {
        'use strict';
        var ret = '';

        for(var i=0; i < classes.length; i++) {
            if(i > 0) {
                ret += ', ';
            }
            ret += "\"" + 'classes/' + classes[i].displayName + "\"";
        }

        if(ret.length > 0 && modules.length > 0) {
            ret += ', ';
        }

        for(var j=0; j < modules.length; j++) {
            if(j > 0) {
                ret += ', ';
            }
            ret += "\"" + 'modules/' + modules[j].displayName + "\"";
        }

        return ret;
    },
    code: function(exampleCode) {
        'use strict';

        function openCodeBlock(language, title) {
            return '\n{code:theme=Midnight' + (language ? ('|language=' + (language === 'html' ? 'xml' : language)) : '') + (title ? ('|title=' + title.trim()) : '') + '}';
        }

        function getLanguage(str) {
            var languageMatch = str.match(/<code class="language-(.*?)"/);
            return languageMatch && languageMatch[1] || '';
        }

        function reparseHtmlifiedExample(htmlString) {
            var title = '';
            var open = false;

            return htmlString.split('\n').reduce(function(str, line, index, strArray) {
                var tailingCode = null;
                var close = '';

                if((/<p>.*?<\/p>/).test(line) && !title && !open && (/<pre.*><code/).test(strArray[index + 1] || '')) {
                    title = line.replace(/<\/?p>/g, '');
                    return str;
                }

                if(!open && (/<pre.*><code/).test(line)) {
                    open = true;
                    tailingCode = line.match(/<code.*?>(.*?$)/);
                    return [str, openCodeBlock(getLanguage(line), title), unescapeHtml(tailingCode && tailingCode[1] || '')].join('\n');
                } else {
                    if(index === strArray.length - 1) {
                        title = '';
                        close = open ? '{code}\n' : '';
                        open = false;
                    }
                    return str + ' ' + (!open ? htmlToMarkdown(line) : ('\n' + unescapeHtml(line))) + close;
                }

                return str;
            }, '');
        }

        return exampleCode.split('</code></pre>').reduce(function(endString, singleExampleString) {
            return endString + reparseHtmlifiedExample(singleExampleString);
        }, '');
    },
    byeByeWhiteSpace: function(options) {
        return options.fn(this).replace(/\s|\t/g, '');
    },
    singleSpacesOnly: function(options) {
        return options.fn(this).split('\n').map(function(line) {
            return line.trim();
        }).join(' ').replace(/(\s|\t)+/, ' ');
    },
    trim: function(options) {
        return options.fn(this).trim();
    },
    lineTrim: function(options) {
        return options.fn(this).split('\n').map(function(line) { return line.trim(); }).join('\n');
    },
    trimmedSingleLineBreaks: function(options) {
        return options.fn(this).replace(/^(\s|\t)*/mg, '').replace(/(\s|\t)*$/mg, '').replace(/\n+/g, '\n');
    },
    htmlToMarkdown: function(options) {
        return htmlToMarkdown(options.fn(this));
    },
    aToMarkup: function(options) {
        return convertATagtoMarkupLink(options.fn(this));
    },
    unescapeCodeblock: function(options) {
        return unescapeHtml(options.fn(this)).replace(/&#x60;/g, '`').replace(/&#x27;/g, '\'').replace(/<\/code>/g, '`');
    },
    linkElementList: function(options) {
        return options.fn(this).split(' ').map(function(element) {
            var el = element.replace(/&lt;|</, '').replace(/&gt;|>/, '').replace(',', '');
            return '[' + el + '|' + urlToWikiUrl('../elements/' + el + '.html') + ']';
        }).join(', ');
    }
};

function convertATagtoMarkupLink(str) {
    'use strict';
    var components = unescapeHtml(str).match(/href="(.*?)".*?><?(.*?)>?<\/a/);
    if(components && components[1] && components[2]) {
        return '[' + components[2] + '|' + urlToWikiUrl(components[1]) + ']';
    } else if(/\.\.\//.test(str)) {
        return urlToWikiUrl(str);
    } else {
        // console.log('could not find crossLink for ' + str);
        return str;
    }
}

function urlToWikiUrl(str) {
    return str;
    // no longer editing this value - can be handled by grunt-confluence-upload
    // otherwise there is a hidden cross package dependency
    // var split = str.split('/');
    // var prefix = titlePrefixes[split[1]];
    // if(/\.\.\//.test(str) && prefix) {
    //     return prefix + split[2].split('.html')[0];
    // } else {
    //     return str;
    // }
}

function htmlToMarkdown(str) {
    return str
        .replace(/\n{2,}/g, '</p>')
        .replace(/\n/g, ' ')
        .replace(/(<|&lt;)\/?(ul|ol)(>|&gt;)/g, '')
        .replace(/(<|&lt;)li(>|&gt;)/g, '* ')
        .replace(/(<|&lt;)\/li(>|&gt;)/g, '')
        .replace(/(<|&lt;)\/?strong(>|&gt;)/g, '*')
        .replace(/(<|&lt;)\/?em(>|&gt;)/g, '_')
        .replace(/(?:<|&lt;)code(?:>|&gt;)(.*?)(?:<|&lt;)\/code(>|&gt;)/g, function(a, b) { return '{{' + unescapeHtml(b) + '}}'; })
        .replace(/(<|&lt;)br ?\/?(>|&gt;)/g, '')
        .replace(/(<|&lt;)a .*?(<|&lt;)\/a(>|&gt;)/g, function(a) {
            return convertATagtoMarkupLink(a);
        })
        .replace(/(<|&lt;)p(>|&gt;)/g, '')
        .replace(/(<|&lt;)\/p(>|&gt;)/g, '\n\n')
        .replace(/<(h\d)>(.*?)<\/h\d>/, '$1.$2\n');
}

function unescapeHtml(str) {
    return str
        .replace(/&amp;/g, '&')
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&quot;/g, '"');
}
