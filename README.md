Confluence Theme for YUIDoc
===========================

A simple theme that will write your YUIDocs in confluence wiki markup.

It is a little rough and ready - created in a hurry for a project that has a pretty specific use case.

Usage
=====

`npm install yuidoc-confluence-theme`

You can then use the directory `node_modules/yuidoc-confluence-theme/theme` to generate the confluence content. You will also need to add `node_modules/yuidoc-confluence-theme/theme/helpers/helpers` to the helpers so that they can format some of the text appropriately.

To get the generated content in to a space on Confluence you can either do it manually (yawn) or there is a grunt task available [here](https://bitbucket.org/coulson84/grunt-confluence-yuidoc/) that will upload all content within a directory up to a space under an (optional) title page